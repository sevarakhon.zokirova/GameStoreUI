import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { IGame } from 'src/app/shared/models/game/game';
import { IAddGameGenres } from 'src/app/shared/models/genre/addGameGenres';
import { IAddGameSubGenres } from 'src/app/shared/models/genre/addGameSubGenres';
import { IGenre } from 'src/app/shared/models/genre/genre';
import { GameGenresService } from '../services/game-genres.service';
import { GameService } from '../services/game.service';
import { GenreService } from '../services/genre.service';

@Component({
  selector: 'app-game-add',
  templateUrl: './game-add.component.html',
  styleUrls: ['./game-add.component.css']
})
export class GameAddComponent implements OnInit
{
  addGroupForm!: FormGroup;
  addedGame!: IGame

  genres!: IGenre[];
  subGenres!: IGenre[];
  selectedGenres = [];
  selectedSubGenres = [];
  formData = new FormData();

  constructor(private gameService: GameService,
    private genreService: GenreService,
    private gameGenresService: GameGenresService,
    private router: Router) { }

  ngOnInit(): void
  {
    this.getGenres();
    this.getSubGenres();
    this.createGameForm();
  }

  createGameForm()
  {
    this.addGroupForm = new FormGroup(
      {
        name: new FormControl('', [Validators.required, Validators.minLength(3)]),
        description: new FormControl('', [Validators.required, Validators.minLength(10)]),
        price: new FormControl('', [Validators.required, Validators.min(1)]),
        image: new FormControl('', Validators.required),
        dropdown: new FormControl('', Validators.required)
      });
  }

  onSubmit()
  {
    this.gameService.addGame(this.addGroupForm.value)
      .subscribe(
        {
          next: response =>
          {
            this.addedGame = response;

            this.addGenres();
            this.addSubGenres();
            this.addPhoto();
          },
          error: error => console.error(error)
        });
  }

  getGenres()
  {
    this.genreService.getGenres().subscribe(
      {
        next: response => this.genres = response,
        error: error => console.error(error)
      });
  }

  getSubGenres()
  {
    this.genreService.getSubGenres().subscribe(
      {
        next: response => this.subGenres = response,
        error: error => console.error(error)
      });
  }

  uploadImage = (image: any) =>
  {
    if (image.length === 0)
    {
      return;
    }

    const imageToUpload = <File>image[0];
    this.formData.append('file', imageToUpload, imageToUpload.name);
  }

  addPhoto()
  {
    this.gameService.addPhoto(this.addedGame.id, this.formData)
      .subscribe(
        {
          next: response =>
          {
            this.router.navigate(['games'])
              .then(() =>
              {
                window.location.reload();
              });
          },
          error: error => console.log(error)
        });
  }

  addGenres()
  {
    if (this.selectedGenres)
    {

      for (let i = 0; i < this.selectedGenres.length; i++)
      {
        const gameGenres: IAddGameGenres = {
          gameId: this.addedGame.id,
          genreId: this.selectedGenres[i]
        }
        this.gameGenresService.assignGenres(gameGenres)
          .subscribe({
            next: response => console.log(response),
            error: error => console.error(error)
          });
      }
    }
  }

  addSubGenres()
  {
    if (this.selectedSubGenres)
    {
      for (let i = 0; i < this.selectedSubGenres.length; i++)
      {
        const gameGenres: IAddGameSubGenres = {
          gameId: this.addedGame.id,
          subGenreId: this.selectedSubGenres[i]
        }
        this.gameGenresService.assignSubGenres(gameGenres)
          .subscribe({
            next: response => console.log(response),
            error: error => console.error(error)
          });
      }
    }
  }
}
