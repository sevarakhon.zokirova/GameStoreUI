import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { IGame } from '../shared/models/game/game';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { IGenre } from '../shared/models/genre/genre';
import { GenreService } from './services/genre.service';
import { GameService } from './services/game.service';
import { GameParams } from '../shared/models/game/gameParams';


@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.css']
})
export class StoreComponent implements OnInit
{
  @ViewChild('search', { static: false }) searchParam!: ElementRef;
  faSearch = faSearch;
  genres?: IGenre[];
  subGenres?: IGenre[];
  games: IGame[] = [];
  params = new GameParams();

  constructor(private genreService: GenreService,
    private gameService: GameService) { }

  ngOnInit(): void
  {
    this.getGames();
    this.getGenres();
    this.getSubGenres();
  }

  getGenres()
  {
    this.genreService.getGenres().subscribe({
      next: response => this.genres = [{ id: 0, name: 'All' }, ...response],
      error: error => console.error(error)
    });
  }

  getSubGenres()
  {
    this.genreService.getSubGenres().subscribe({
      next: response => this.subGenres = [{ id: 0, name: 'All' }, ...response],
      error: error => console.error(error)
    });
  }

  onGenreSelected(genreId: number)
  {
    this.params.genreId = genreId;
    this.getGames();
  }

  onSubGenreSelected(subGenreId: number)
  {
    this.params.subGenreId = subGenreId;
    this.getGames();
  }

  onSearch()
  {
    if (this.searchParam.nativeElement.value !== '')
    {
      this.params.name = this.searchParam.nativeElement.value;
      this.getGames();
    }
    else
    {
      this.params = new GameParams();
      this.getGames();
    }
  }

  getGames()
  {
    this.gameService.getGames(this.params)
      .subscribe({
        next: response => 
        {
          if (response)
          {
            this.games = response;
          }
        },
        error: error => console.error(error)
      });
  }
}
