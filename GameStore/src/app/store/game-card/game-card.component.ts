import { Component, Input, OnInit, TemplateRef } from '@angular/core';
import { IGame } from 'src/app/shared/models/game/game';
import { GameService } from '../services/game.service';
import { faInfo } from '@fortawesome/free-solid-svg-icons';
import { faPencil } from '@fortawesome/free-solid-svg-icons';
import { faTrash } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { GameGenresService } from '../services/game-genres.service';
import { IGenre } from 'src/app/shared/models/genre/genre';

@Component({
  selector: 'app-game-card',
  templateUrl: './game-card.component.html',
  styleUrls: ['./game-card.component.css']
})
export class GameCardComponent implements OnInit
{
  @Input() game!: IGame;

  modalRef?: BsModalRef;

  gameGenres: IGenre[] = [];
  gameSubGenres: IGenre[] = [];

  faInfo = faInfo;
  faPencil = faPencil;
  faTrash = faTrash;

  constructor(private gameService: GameService,
    private gameGenresService: GameGenresService,
    private router: Router,
    private modalService: BsModalService) { }

  ngOnInit(): void
  {
    this.gameGenresService.getGameGenres(this.game.id).subscribe({
      next: response => this.gameGenres = response,
      error: error => console.error(error)
    });
    this.gameGenresService.getGameSubGenres(this.game.id).subscribe({
      next: response => this.gameSubGenres = response,
      error: error => console.error(error)
    });
  }

  getAllGenres()
  {
    return this.gameGenres.map(x => (x.name))
      .concat(this.gameSubGenres.map(x => x.name))
      .join(' / ');
  }

  deleteGame(id: number)
  {
    this.gameService.deleteGame(id).subscribe({
      next: response =>
      {
        window.location.reload();
        this.router.navigate(['/'])
      },
      error: error => console.error(error)
    });
  }

  openModal(template: TemplateRef<any>)
  {
    this.modalRef = this.modalService.show(template, { class: 'modal-sm' });
  }

  confirm()
  {
    this.deleteGame(this.game.id);
    this.modalRef?.hide();
  }

  decline()
  {
    this.modalRef?.hide();
  }
}
