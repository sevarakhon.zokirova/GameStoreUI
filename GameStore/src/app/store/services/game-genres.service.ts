import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IAddGameGenres } from 'src/app/shared/models/genre/addGameGenres';
import { IAddGameSubGenres } from 'src/app/shared/models/genre/addGameSubGenres';
import { IGenre } from 'src/app/shared/models/genre/genre';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GameGenresService
{
  addGameGenresUrl: string = 'gameGenres/assignGenres';
  addGameSubGenresUrl: string = 'GameGenres/assignSubGenres';
  gameGenresUrl: string = 'gameGenres/';
  gameSubGenresUrl: string = 'gameGenres/gameSubGenres/';


  constructor(private http: HttpClient)
  {
  }

  assignGenres(gameGenres: IAddGameGenres)
  {
    return this.http.post(environment.baseUrl + this.addGameGenresUrl, gameGenres);
  }

  assignSubGenres(gameGenres: IAddGameSubGenres)
  {
    return this.http.post(environment.baseUrl + this.addGameSubGenresUrl, gameGenres);
  }


  getGameGenres(id: number)
  {
    return this.http.get<IGenre[]>(environment.baseUrl + this.gameGenresUrl + id);
  }

  getGameSubGenres(id: number)
  {
    return this.http.get<IGenre[]>(environment.baseUrl + this.gameSubGenresUrl + id);
  }
}
