import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IGame } from 'src/app/shared/models/game/game';
import { GameParams } from 'src/app/shared/models/game/gameParams';
import { IPhoto } from 'src/app/shared/models/photo/photo';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GameService
{
  constructor(private http: HttpClient) { }

  gameUrl: string = 'game';
  gamePhotoUploadUrl: string = '/add-photo/'
  gamePhotoUpdateUrl: string = '/update-photo/';

  getGames(gameParams: GameParams)
  {
    let params = new HttpParams();

    if (gameParams.genreId !== 0)
    {
      params = params.append('genreId', gameParams.genreId);
      console.log(params.get('genreId'));
    }

    if (gameParams.subGenreId !== 0)
    {
      params = params.append('subGenreId', gameParams.subGenreId);
    }

    if (gameParams.name)
    {
      params = params.append('name', gameParams.name);
    }

    return this.http.get<IGame[]>(environment.baseUrl + this.gameUrl, { params: params });
  }

  getGameDetails(id: string)
  {
    return this.http.get<IGame>(environment.baseUrl + this.gameUrl + '/info/' + id);
  }

  getGameById(id: number)
  {
    return this.http.get<IGame>(environment.baseUrl + this.gameUrl + '/' + id);
  }

  addGame(game: IGame)
  {
    return this.http.post<IGame>(environment.baseUrl + this.gameUrl, game);
  }

  updateGame(id: number, game: IGame)
  {
    return this.http.put<IGame>(environment.baseUrl + this.gameUrl + '/' + id, game)
  }

  deleteGame(id: number)
  {
    return this.http.delete(environment.baseUrl + this.gameUrl + '/' + id)
  }

  addPhoto(id: number, image: any)
  {
    return this.http.post<IPhoto>(environment.baseUrl + this.gameUrl + this.gamePhotoUploadUrl + id, image)
  }

  updatePhoto(id: number, image: any)
  {
    return this.http.put<IPhoto>(environment.baseUrl + this.gameUrl + this.gamePhotoUpdateUrl + id, image);
  }
}
