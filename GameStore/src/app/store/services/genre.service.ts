import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IGenre } from 'src/app/shared/models/genre/genre';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GenreService
{
  genreUrl: string = 'genre'
  subGenreUrl: string = 'subGenre';

  constructor(private http: HttpClient)
  { }

  getGenres()
  {
    return this.http.get<IGenre[]>(environment.baseUrl + this.genreUrl);
  }

  getSubGenres()
  {
    return this.http.get<IGenre[]>(environment.baseUrl + this.subGenreUrl);
  }
}
