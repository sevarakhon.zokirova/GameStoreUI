import { Component, OnInit } from '@angular/core';
import { IGame } from 'src/app/shared/models/game/game';
import { GameService } from '../services/game.service';


@Component({
  selector: 'app-game-list',
  templateUrl: './game-list.component.html',
  styleUrls: ['./game-list.component.css']
})
export class GameListComponent implements OnInit
{
  games?: IGame[];
  private url: string = "game"

  constructor(private storeService: GameService) { }

  ngOnInit(): void
  {

  }

}
