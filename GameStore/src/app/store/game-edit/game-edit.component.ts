import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { IGame } from 'src/app/shared/models/game/game';
import { IGenre } from 'src/app/shared/models/genre/genre';
import { GameGenresService } from '../services/game-genres.service';
import { GameService } from '../services/game.service';
import { GenreService } from '../services/genre.service';

@Component({
  selector: 'app-game-edit',
  templateUrl: './game-edit.component.html',
  styleUrls: ['./game-edit.component.css']
})
export class GameEditComponent implements OnInit
{
  editGroupForm!: FormGroup;
  game!: IGame;
  id!: number;
  genres!: IGenre[];
  subGenres!: IGenre[];
  gameGenres: number[] = [];
  gameSubGenres: number[] = [];
  selectedGameGenres?: IGenre[];
  selectedGameSubGenres?: IGenre[];
  formData = new FormData();

  constructor(private gameService: GameService,
    private gameGenresService: GameGenresService,
    private genreService: GenreService,
    private router: Router,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void
  {
    this.loadGame();
    this.getGenres();
    this.getSubGenres();
    this.getGameGenres();
    this.getGameSubGenres();
    this.createGameForm();
  }

  createGameForm()
  {
    this.editGroupForm = new FormGroup(
      {
        name: new FormControl('', [Validators.required, Validators.minLength(3)]),
        description: new FormControl('', [Validators.required, Validators.minLength(10)]),
        price: new FormControl('', [Validators.required, Validators.min(1)])
      });
  }

  loadGame()
  {
    let idParam = this.activatedRoute.snapshot.paramMap.get('id');
    this.id = idParam ? +idParam : 0;

    this.gameService.getGameById(this.id).subscribe(
      {
        next: response => 
        {
          this.game = response;
          this.editGroupForm.patchValue(this.game);
        },
        error: error => console.error(error)
      });
  }

  onSubmit()
  {
    let updatedGame = this.game;
    updatedGame.id = this.id;
    updatedGame.name = this.editGroupForm.controls['name'].value;
    updatedGame.price = this.editGroupForm.controls['price'].value;
    updatedGame.description = this.editGroupForm.controls['description'].value;

    this.gameService.updateGame(this.id, updatedGame)
      .subscribe(
        {
          next: response => 
          {
            this.router.navigate(['games'])
              .then(() => 
              {
                window.location.reload();
              })
          },
          error: error => console.error(error)
        });
  }

  uploadImage = (image: any) =>
  {
    if (image.length === 0)
    {
      return;
    }

    const imageToUpload = <File>image[0];
    this.formData.append('file', imageToUpload, imageToUpload.name);
  }

  updatePhoto()
  {
    this.gameService.updatePhoto(this.id, this.formData)
      .subscribe(
        {
          next: response =>
          {
            this.router.navigate(['games'])
              .then(() =>
              {
                window.location.reload();
              });
          },
          error: error => console.log(error)
        });
  }

  onPhotoSubmit()
  {
    this.updatePhoto();
  }

  getGameGenres()
  {
    this.gameGenresService.getGameGenres(this.id)
      .subscribe({
        next: response =>
        {
          for (let i = 0; i < response.length; i++)
          {
            const genreName = response[i].name;
            const genre = this.genres.filter(x => x.name === genreName)[0];
            this.gameGenres.push(genre.id);
          }
          console.log(this.gameGenres);
        },
        error: error => console.error(error)
      });
  }

  getGameSubGenres()
  {
    this.gameGenresService.getGameSubGenres(this.id)
      .subscribe({
        next: response =>
        {
          for (let i = 0; i < response.length; i++)
          {
            const genreName = response[i].name;
            const genre = this.subGenres.filter(x => x.name === genreName)[0];
            this.gameSubGenres.push(genre.id);
          }
          console.log(this.gameSubGenres);
        },
        error: error => console.error(error)
      });
  }

  getGenres()
  {
    this.genreService.getGenres()
      .subscribe({
        next: response => this.genres = response,
        error: error => console.error(error)
      });
  }

  getSubGenres()
  {
    this.genreService.getSubGenres()
      .subscribe({
        next: response => this.subGenres = response,
        error: error => console.error(error)
      });
  }

  onGenreSubmit()
  {
    console.log(this.gameGenres);
    console.log(this.gameSubGenres);
  }
}
