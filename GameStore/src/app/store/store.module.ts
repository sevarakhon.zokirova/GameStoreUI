import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreComponent } from './store.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { GameCardComponent } from './game-card/game-card.component';
import { GameListComponent } from './game-list/game-list.component';
import { GameDetailsComponent } from './game-details/game-details.component';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { GameAddComponent } from './game-add/game-add.component';
import { GameEditComponent } from './game-edit/game-edit.component';
import { SharedModule } from '../shared/shared.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AccordionModule } from 'ngx-bootstrap/accordion';

@NgModule({
  declarations: [
    StoreComponent,
    GameListComponent,
    GameCardComponent,
    GameDetailsComponent,
    GameAddComponent,
    GameEditComponent
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    RouterModule,
    BrowserAnimationsModule,
    BsDropdownModule.forRoot(),
    SharedModule,
    NgSelectModule,
    FormsModule,
    ModalModule.forRoot(),
    AccordionModule.forRoot()
  ],
  exports: [StoreComponent]
})
export class StoreModule { }
