import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IGame } from 'src/app/shared/models/game/game';
import { IGenre } from 'src/app/shared/models/genre/genre';
import { GameGenresService } from '../services/game-genres.service';
import { GameService } from '../services/game.service';

@Component({
  selector: 'app-game-details',
  templateUrl: './game-details.component.html',
  styleUrls: ['./game-details.component.css']
})
export class GameDetailsComponent implements OnInit
{
  game!: IGame;
  gameGenres: IGenre[] = [];
  gameSubGenres: IGenre[] = [];

  constructor(private gameService: GameService,
    private gameGenresService: GameGenresService,
    private activateRoute: ActivatedRoute) { }

  ngOnInit(): void
  {
    this.loadGameDetails();

  }

  loadGameDetails()
  {
    const id = this.activateRoute.snapshot.paramMap.get('id');
    if (id)
    {
      this.gameService.getGameDetails(id)
        .subscribe({
          next: response =>
          {
            this.game = response;
            this.loadGameGenres()
          },
          error: error => console.error(error)

        });
    }
  }

  loadGameGenres()
  {
    this.gameGenresService.getGameGenres(this.game.id).subscribe({
      next: response => this.gameGenres = response,
      error: error => console.error(error)
    });
    this.gameGenresService.getGameSubGenres(this.game.id).subscribe({
      next: response => this.gameSubGenres = response,
      error: error => console.error(error)
    });
  }

  getGameGenres()
  {
    return this.gameGenres.concat(this.gameSubGenres).map(x => (x.name));
  }
}
