export interface IAddGameSubGenres
{
    gameId: number;
    subGenreId: number;
}