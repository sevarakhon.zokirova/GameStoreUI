export interface SubGenre
{
    id: number;
    name: string;
    genreId: number;
}