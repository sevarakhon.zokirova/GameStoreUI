export interface IGameGenre
{
    id: number,
    genre: string
}