export interface IGame
{
    id: number,
    name: string,
    description: string,
    price: number | string,
    photoUrl?: string
}