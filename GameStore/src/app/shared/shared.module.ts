import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReactiveFormsModule } from '@angular/forms';
import { TextInputComponent } from './components/forms/text-input/text-input.component';
import { TextareaComponent } from './components/forms/textarea/textarea.component';

@NgModule({
  declarations: [
    TextInputComponent,
    TextareaComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [
    ReactiveFormsModule,
    TextInputComponent,
    TextareaComponent]
})
export class SharedModule { }
