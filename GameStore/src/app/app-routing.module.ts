import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GameAddComponent } from './store/game-add/game-add.component';
import { GameDetailsComponent } from './store/game-details/game-details.component';
import { GameEditComponent } from './store/game-edit/game-edit.component';
import { StoreComponent } from './store/store.component';

const routes: Routes = [
  { path: '', component: StoreComponent },
  { path: 'games', component: StoreComponent },
  { path: 'games/view/:id', component: GameDetailsComponent },
  { path: 'games/edit/:id', component: GameEditComponent },
  { path: 'games/add', component: GameAddComponent },
  { path: '**', redirectTo: '', pathMatch: 'full' }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
